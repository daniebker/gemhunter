package lib.states;

import com.badlogic.gdx.utils.Logger;
import com.daniebker.gemhunter.definitions.AppSettings;
import com.daniebker.gemhunter.states.StateType;
import lib.sprites.GameActor;

/**
 * Created by dan on 29/12/14.
 */
public abstract class State {

    private final Logger logger;
    private float stateTime;
    private StateType animationType;

    public float getStateTime() { return stateTime; }

    public StateType getStateType(){

        return animationType;
    }

    public State(StateType animationType) {

        this.animationType = animationType;
        this.logger = new Logger(this.getClass().getName(), AppSettings.LOGGING_LEVEL);
    }

    public void enter(GameActor gameActor) {
        stateTime = 0;
        logger.info("Entering: " + getStateType().toString());
    }

    public void update(float deltaTime, GameActor gameActor) {
        stateTime += deltaTime;
    }

    public void exit(GameActor gameActor) {

        logger.info("Exiting: " + getStateType().toString());
    }

    @Override
    public boolean equals(Object other) {

        if(!(other instanceof  State)) {
            return false;
        }

        State rhs = (State)other;
        if(this.getStateType() == rhs.getStateType())
        {
            return true;
        }

        return false;
    }
}

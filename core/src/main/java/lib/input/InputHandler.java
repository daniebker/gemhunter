package lib.input;

import com.badlogic.gdx.utils.Array;

/**
 * Created by dan on 10/12/14.
 */
public interface InputHandler {
    Array<Command> getCommands();
}
package lib.input;

import lib.sprites.GameActor;

/**
 * Interface for implementing a Command
 */
public interface Command {

    /**
     * Execute this command on the provided game actor
     * @param gameActor the GameActor to update the command on
     */
    void execute(GameActor gameActor);

    /**
     * Whether or not this command has been handled
     */
    boolean wasHandled();
}
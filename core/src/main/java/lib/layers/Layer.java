package lib.layers;

import com.badlogic.gdx.graphics.g2d.Batch;

/**
 * Created by dan on 30/12/14.
 */
public interface Layer {

    void create();

    void render(Batch batch);

    void update(float deltaTime);
}

package lib;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.daniebker.gemhunter.definitions.LevelDefinitions;
import com.daniebker.gemhunter.definitions.TextureDefinitions;

/**
 * Created by dan on 14/03/15.
 */
public class AssetLoader {

    private AssetManager assetManager;
    public AssetManager getAssetManager() {
        return assetManager;
    }

    /**
     * Responsible for loading all assets.
     * @param assetManager
     */
    public AssetLoader(AssetManager assetManager) {

        this.assetManager = assetManager;
    }

    /**
     * Currently loads all assets for the game.
     * TODO: Should instead load only the required assets for a level.
     */
    public void loadAssets() {

        assetManager.clear();

        // only needed once
        assetManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));

        //Levels
        assetManager.load(LevelDefinitions.LEVEL3, TiledMap.class);

        //Textures
        assetManager.load(TextureDefinitions.BADLOGIC, Texture.class);

        //PLAYER
        assetManager.load(TextureDefinitions.IDLE0, Texture.class);
        assetManager.load(TextureDefinitions.IDLE1, Texture.class);
        assetManager.load(TextureDefinitions.IDLE2, Texture.class);
        assetManager.load(TextureDefinitions.IDLE3, Texture.class);
        assetManager.load(TextureDefinitions.IDLE4, Texture.class);
        assetManager.load(TextureDefinitions.IDLE5, Texture.class);
        assetManager.load(TextureDefinitions.IDLE6, Texture.class);
        assetManager.load(TextureDefinitions.IDLE7, Texture.class);
        assetManager.load(TextureDefinitions.IDLE8, Texture.class);
        assetManager.load(TextureDefinitions.IDLE9, Texture.class);

        assetManager.load(TextureDefinitions.RUN0, Texture.class);
        assetManager.load(TextureDefinitions.RUN1, Texture.class);
        assetManager.load(TextureDefinitions.RUN2, Texture.class);
        assetManager.load(TextureDefinitions.RUN3, Texture.class);
        assetManager.load(TextureDefinitions.RUN4, Texture.class);
        assetManager.load(TextureDefinitions.RUN5, Texture.class);
        assetManager.load(TextureDefinitions.RUN6, Texture.class);
        assetManager.load(TextureDefinitions.RUN7, Texture.class);
        assetManager.load(TextureDefinitions.RUN8, Texture.class);
        assetManager.load(TextureDefinitions.RUN9, Texture.class);

        assetManager.load(TextureDefinitions.JUMP0, Texture.class);
        assetManager.load(TextureDefinitions.JUMP1, Texture.class);
        assetManager.load(TextureDefinitions.JUMP2, Texture.class);
        assetManager.load(TextureDefinitions.JUMP3, Texture.class);
        assetManager.load(TextureDefinitions.JUMP4, Texture.class);
        assetManager.load(TextureDefinitions.JUMP5, Texture.class);
        assetManager.load(TextureDefinitions.JUMP6, Texture.class);
        assetManager.load(TextureDefinitions.JUMP7, Texture.class);
        assetManager.load(TextureDefinitions.JUMP8, Texture.class);
        assetManager.load(TextureDefinitions.JUMP9, Texture.class);

        assetManager.load(TextureDefinitions.SLIDE0, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE1, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE2, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE3, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE4, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE5, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE6, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE7, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE8, Texture.class);
        assetManager.load(TextureDefinitions.SLIDE9, Texture.class);

        assetManager.finishLoading();
    }
}

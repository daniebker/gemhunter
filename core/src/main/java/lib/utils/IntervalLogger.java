package lib.utils;

import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.TimeUtils;

/**
 * Created by dan on 31/12/14.
 */
public class IntervalLogger extends Logger {

    private long startTime;

    /* 1,000,000,000ns == one second */
    private static int INTERVAL = 1000000000;

    public IntervalLogger(String tag) {
        super(tag);
        startTime = TimeUtils.nanoTime();
    }

    public IntervalLogger(Object o, int level) {
        super(o.getClass().getSimpleName(), level);
        startTime = TimeUtils.nanoTime();

    }

    @Override
    public void debug (String message) {
        if (canLog()) {
           super.debug(message);
            startTime = TimeUtils.nanoTime();
        }
    }

    @Override
    public void debug (String message, Exception exception) {
        if (canLog()) {
            super.debug(message);
            startTime = TimeUtils.nanoTime();
        }
    }

    @Override
    public void info (String message) {
        if (canLog()) {
            super.info(message);
            startTime = TimeUtils.nanoTime();
        }
    }

    @Override
    public void info (String message, Exception exception) {
        if (canLog()) {
            super.info(message, exception);
            startTime = TimeUtils.nanoTime();
        }
    }

    @Override
    public void error (String message) {
        if (canLog()) {
            super.error(message);
            startTime = TimeUtils.nanoTime();
        }
    }

    @Override
    public void error (String message, Throwable exception) {
        if (canLog()) {
            super.error(message, exception);
            startTime = TimeUtils.nanoTime();
        }
    }

    private boolean canLog() {
        return TimeUtils.nanoTime() - startTime > INTERVAL;
    }
}

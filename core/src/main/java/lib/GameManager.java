package lib;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.daniebker.gemhunter.Director;

/**
 * Created by dan on 14/03/15.
 */
public interface GameManager {
    SpriteBatch getBatch();

    AssetLoader getAssetLoader();

    Director getDirector();
}

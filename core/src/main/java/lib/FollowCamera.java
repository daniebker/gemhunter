package lib;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.daniebker.gamelib.Observable;
import com.daniebker.gamelib.cameras.ObservableGameCamera;
import lib.sprites.GameActor;

/**
 * Created by dan on 31/12/14.
 */
public class FollowCamera implements ObservableGameCamera {

    private static final int SCREEN_WIDTH = 32;
    private static final int SCREEN_HEIGHT = 18;
    private final OrthographicCamera camera;
    private final float worldWidth;
    private final float worldHeight;

    public FollowCamera(OrthographicCamera camera, float worldWidth, float worldHeight) {
        this.camera = camera;
        this.worldWidth = worldWidth;
        this.worldHeight = worldHeight;
        reset();
    }

    @Override
    public void reset() {
        getPosition().set(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2, 0);
    }

    @Override
    public OrthographicCamera getOrthographicCamera() {
        return camera;
    }

    @Override
    public void translate(int x, int y, int z) {
        camera.translate(x, y);
    }

    @Override
    public void rotate(float angle, int x, int y, int z) {
        camera.rotate(angle, x, y, z);
    }

    @Override
    public float getZoom() {
        return camera.zoom;
    }

    @Override
    public float getViewportWidth() {
        return camera.viewportWidth;
    }

    @Override
    public float getViewportHeight() {
        return camera.viewportHeight;
    }

    @Override
    public void zoom(float v) {
        camera.zoom = v;
    }

    @Override
    public Vector3 getPosition() {
        return camera.position;
    }

    @Override
    public float getScreenWidth() {
        return SCREEN_WIDTH;
    }

    @Override
    public float getScreenHeight() {
        return SCREEN_HEIGHT;
    }

    @Override
    public void update(Observable observable) {

        GameActor gameActor = (GameActor)observable;

        getPosition().y = gameActor.getY();
        getPosition().x = gameActor.getX();

        if(getPosition().x < getViewportWidth()/2f) {
            getPosition().x = getViewportWidth()/2f;
        }
        else if (getPosition().x > worldWidth - getViewportWidth()/2f) {
            getPosition().x = worldHeight - getViewportWidth()/2f;
        }

        if(getPosition().y < getViewportHeight()/2f) {
            getPosition().y = getViewportHeight()/2f;
        }
        else if(getPosition().y > worldWidth- getViewportHeight()/2f) {
            getPosition().y = worldHeight - getViewportHeight()/2f;
        }
    }
}
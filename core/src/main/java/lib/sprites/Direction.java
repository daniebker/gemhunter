package lib.sprites;

/**
 * Created by dan on 17/03/15.
 */
public enum Direction {
    LEFT,
    RIGHT,
    STOPPED
}

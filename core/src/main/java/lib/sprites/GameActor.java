package lib.sprites;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.daniebker.gemhunter.states.StateType;
import lib.states.State;

/**
 * Represents an intractable game actor
 */
public interface GameActor {

    /**
     * Gets the Velocity of the current
     * Body for this Actor
     *
     * @return Box2D Body Velocity
     */
    Vector2 getVelocity();

    /**
     * Sets whether or not the current Actor
     * is grounded
     *
     * @param value value to set
     */
    void setIsGrounded(boolean value);

    /**
     * Gets the value of isGrounded;
     * @return Whether the current actor is
     *
     * on the ground
     */
    boolean getIsGrounded();

    /**
     * Gets the current Box2D body representing
     * this actor
     *
     * @return Box2D Body
     */
    Body getBody();

    /**
     * Gets the current X coordinate
     * of the Current Box2D Body
     *
     * @return X coordinate of the Actor
     */
    public float getX();

    /**
     * Gets the current Y coordinate
     * of the current Box2dActor
     *
     * @return Y coordinates of the actor
     */
    public float getY();

    /**
     * Gets the current State of the
     * actor
     *
     * @return current State
     */
    public State getState();

    /**
     * Responsible for drawing the actor
     *
     * @param batch SpriteBatch to use
     *              for rendering
     */
    void render(Batch batch);

    /**
     * Update this actor
     *
     * @param deltaTime since the last update
     */
    void update(float deltaTime);

    boolean getIsDestroyed();
    void destroy();

    boolean getCanStand();

    void setJumpHasPeaked(boolean value);

    void changeState(State playerState);

    float getMaxVelocity();

    float getHeight();

    float getWidth();

    boolean getFacesRight();

    Vector2 getForceVector();

    void setFacesRight(boolean value);

    Direction getDirection();

    void setDirection(Direction value);

    void setCurrentAnimationByState(StateType currentAnimation);

    void setCanStand(boolean value);

    boolean getJumpHasPeaked();

    void setDodgeEnd(boolean value);

    boolean getDodgeEnd();
}

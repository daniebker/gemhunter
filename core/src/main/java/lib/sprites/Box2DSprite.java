package lib.sprites;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.daniebker.gemhunter.CollisionResolver;
import com.daniebker.gemhunter.states.StateType;
import lib.states.State;

/**
 * Reprentes an interactable sprite in the game
 * Uses Box2D for phyics
 */
public abstract class Box2DSprite implements GameActor, CollisionResolver {

    protected Body body;
    protected Sprite sprite;
    protected State state;
    protected boolean isDestroyed = false;
    protected boolean isGrounded = false;
    private boolean facesRight = true;
    private boolean canStand = true;
    private boolean jumpHasPeaked = false;
    private boolean dodgeEnd = false;
    private Direction direction = Direction.STOPPED;
    protected StateType currentAnimation;

    @Override
    public float getX() { return sprite.getX(); }

    @Override
    public float getY() { return sprite.getY(); }

    @Override
    public float getHeight() { return sprite.getHeight(); }

    @Override
    public float getWidth() { return sprite.getWidth(); }

    @Override
    public State getState() { return state; }

    @Override
    public void setFacesRight(boolean value) {
        facesRight = value;
    }

    @Override
    public Direction getDirection() { return direction; }

    @Override
    public void setDirection(Direction value) { direction = value; }

    public Box2DSprite(Body body, Sprite sprite) {
        this.sprite = sprite;
        this.body = body;
        sprite.setOriginCenter();
        body.setUserData(this);
    }

    @Override
    public void render(Batch batch) {
        if (facesRight) {
            sprite.flip(false, false);
            sprite.draw(batch);
        } else {
            sprite.flip(true, false);
            sprite.draw(batch);
        }
    }

    @Override
    public void update(float deltaTime)
    {
        sprite.setPosition(body.getPosition().x-sprite.getWidth()/2f, body.getPosition().y-sprite.getHeight()/2f);
        sprite.setRotation(body.getAngle() * MathUtils.radiansToDegrees);
    }

    @Override
    public Vector2 getVelocity() {
        return body.getLinearVelocity();
    }

    @Override
    public boolean getFacesRight() { return facesRight; }

    @Override
    public Body getBody() {
        return body;
    }

    @Override
    public boolean getIsDestroyed() {
        return isDestroyed;
    }

    @Override
    public void setIsGrounded(boolean value) {
        isGrounded = value;
    }

    @Override
    public void destroy() { isDestroyed = true; }

    @Override
    public abstract void resolveStartCollision(Fixture fixture);

    @Override
    public boolean getIsGrounded() {
        return isGrounded;
    }

    @Override
    public void setCanStand(boolean value) {

        canStand = value;
    }

    @Override
    public boolean getCanStand() {

        return canStand;
    }

    @Override
    public boolean getJumpHasPeaked() {

        return jumpHasPeaked;
    }

    @Override
    public void setJumpHasPeaked(boolean value) {

        jumpHasPeaked = value;
    }

    @Override
    public void setDodgeEnd(boolean value) {

        dodgeEnd = value;
    }

    @Override
    public boolean getDodgeEnd() {

        return dodgeEnd;
    }

    @Override
    public void changeState(State playerState) {
        if (!state.equals(playerState)) {
            state.exit(this);
            this.state = playerState;
            state.enter(this);
        }
    }

    @Override
    public void setCurrentAnimationByState(StateType value) {
        this.currentAnimation = value;
    }

}

package lib.scenes;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import lib.GameManager;
import lib.layers.Layer;

/**
 * Created by dan on 14/03/15.
 */
public class Scene extends ScreenAdapter {

    private final Array<Layer> layers = new Array<Layer>();
    protected final SpriteBatch batch;

    public Scene(GameManager game) {
        this.batch = game.getBatch();
    }

    protected void addLayer(Layer layer) {
        layer.create();
        layers.add(layer);
    }

    @Override
    public void render(float delta) {

        for(Layer layer : layers) {
            layer.update(delta);
        }

        for(Layer layer : layers) {
            batch.begin();
            layer.render(batch);
            batch.end();
        }

    }



}

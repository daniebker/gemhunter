package lib.physics;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Created by dan on 30/12/14.
 */
public class Box2DBodyBuilder implements BodyBuilder {

    private World world;

    public Box2DBodyBuilder(World world) {
        this.world = world;
    }

    @Override
    public Body createBody(BodyType bodyType) {
        // First we create a body definition
        BodyDef bodyDef = new BodyDef();
        // We set our body to dynamic, for something like ground which doesn't move we would set it to StaticBody
        bodyDef.type = bodyType;
        // Set our body's starting position in the world
        bodyDef.fixedRotation = true;
        // Create our body in the world using our body definition
        return world.createBody(bodyDef);
        }

}

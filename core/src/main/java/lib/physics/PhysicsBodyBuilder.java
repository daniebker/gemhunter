package lib.physics;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.*;

/**
 * Created by dan on 21/02/15.
 */
public class PhysicsBodyBuilder {

    private World world;

    public PhysicsBodyBuilder(World world) {

        this.world = world;
    }

    public Body createBodyFromSprite(Sprite sprite, BodyDef boxBodyDef, FixtureDef fixtureDef) {

        Body boxBody = world.createBody(boxBodyDef);
        PolygonShape boxPoly = new PolygonShape();
        boxPoly.setAsBox(sprite.getWidth()/2f, sprite.getHeight()/2f);

        fixtureDef.shape = boxPoly;

        Fixture boxBodyFixture = boxBody.createFixture(fixtureDef);
        boxBodyFixture.setUserData(sprite);

        boxPoly.dispose();

        return boxBody;
    }
}

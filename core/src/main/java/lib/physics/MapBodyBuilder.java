package lib.physics;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Shape;

/**
 * Created by dan on 14/12/14.
 */
//TODO: Add comments
public interface MapBodyBuilder {
    void createPhysics(TiledMap map);

    void createPhysics(TiledMap map, String layerName);

    Shape createShape(MapObject object);

    void setUpMaterials(FileHandle internal);
}
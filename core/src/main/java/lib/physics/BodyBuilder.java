package lib.physics;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;

/**
 * Created by dan on 14/12/14.
 */
//TODO: Add comments
public interface BodyBuilder {
    Body createBody(BodyType bodyType);
}
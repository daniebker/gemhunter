package lib.physics;

import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.physics.box2d.Shape;

/**
 * Created by dan on 14/12/14.
 */
//TODO: Add comments
public interface ShapeFactory {
    Shape getRectangle(RectangleMapObject rectangleObject);

    Shape getCircle(CircleMapObject circleObject);

    Shape getPolygon(PolygonMapObject polygonObject);

    Shape getPolyline(PolylineMapObject polylineObject);
}
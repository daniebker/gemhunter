package lib.physics;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.objects.*;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.utils.*;
import com.badlogic.gdx.utils.JsonValue.JsonIterator;

import java.util.Iterator;

import static com.daniebker.gemhunter.definitions.AppSettings.LOGGING_LEVEL;
/**
 * @author David Saltares Márquez david.saltares at gmail.com
 * @brief Populates box2D world with static bodies using data from a map object
 *
 * It uses a JSON formatted materials file to assign properties to the static
 * bodies it creates. To assign a material to a shape add a "material" custom
 * property to the shape in question using your editor of choice (Tiled, Gleed,
 * Tide...). Such file uses the following structure:
 @code
 [
 { "name" : "ice", "density" : 1.0, "restitution" : 0.0, "friction" : 0.1 },
 { "name" : "elastic", "density" : 1.0, "restitution" : 0.8, "friction" : 0.8 }
 ]
 @endcode
  * In case no material property is found, it'll get a default one.
 *
 */
public class Box2DMapBodyBuilder implements MapBodyBuilder {

    private Logger logger;
    private Array<Body> bodies = new Array<Body>();
    private ObjectMap<String, FixtureDef> materials = new ObjectMap<String, FixtureDef>();
    private ShapeFactory box2DShapeFactory;
    private BodyBuilder box2DBodyBuilder;

    /**
     *
     * @param box2DShapeFactory
     * @param box2DBodyBuilder
     */
    public Box2DMapBodyBuilder(
            ShapeFactory box2DShapeFactory,
            BodyBuilder box2DBodyBuilder) {
        this.box2DShapeFactory = box2DShapeFactory;
        this.box2DBodyBuilder = box2DBodyBuilder;

        logger = new Logger(this.toString(), LOGGING_LEVEL);
        logger.info("initialising");
    }

    @Override
    public void setUpMaterials(FileHandle materialsFile) {
        logger.info("building default FixtureDefinition");
        FixtureDef defaultFixture = new FixtureDef();
        defaultFixture.density = 1.0f;
        defaultFixture.friction = 10f;
        defaultFixture.restitution = 0.0f;
        materials.put("default", defaultFixture);
        if (materialsFile != null) {
            logger.info("loading materials file");
            loadMaterialsFile(materialsFile);
        }
    }
    /**
     * @param map will use the "physics" layer of this map to look for shapes in order to create the static bodies.
     */
    @Override
    public void createPhysics(TiledMap map) {
        createPhysics(map, "physics");
    }

    /**
     * @param map map to be used to create the static bodies.
     * @param layerName name of the layer that contains the shapes.
     */
    @Override
    //TODO: Tidy up this method
    public void createPhysics(TiledMap map, String layerName) {
        MapLayer layer = map.getLayers().get(layerName);
        if (layer == null) {
            logger.error("layer " + layerName + " does not exist");
            return;
        }
        MapObjects objects = layer.getObjects();
        Iterator<MapObject> objectIt = objects.iterator();
        while(objectIt.hasNext()) {
            MapObject object = objectIt.next();
            if (object instanceof TextureMapObject){
                continue;
            }

            Shape shape = createShape(object);

            MapProperties properties = object.getProperties();
            String material = properties.get("material", "default", String.class);
            String bodyType = properties.get("bodyType", "default", String.class);

            FixtureDef fixtureDef = materials.get(material);
            if (fixtureDef == null) {
                logger.error("material does not exist " + material + " using default");
                fixtureDef = materials.get("default");
            }
            fixtureDef.shape = shape;

            Body body;
            if(bodyType.equalsIgnoreCase("static")) {
                 body = box2DBodyBuilder.createBody(BodyType.StaticBody);
            }
            else if (bodyType.equals("dynamic")) {
                body = box2DBodyBuilder.createBody(BodyType.DynamicBody);
            }
            else {
                body = box2DBodyBuilder.createBody(BodyType.StaticBody);
            }

            body.createFixture(fixtureDef);
            body.setUserData(object);
            bodies.add(body);
            fixtureDef.shape = null;
            shape.dispose();
        }
    }

    public Shape createShape(MapObject object) {
        Shape shape = null;

        if (object instanceof RectangleMapObject) {
            RectangleMapObject rectangle = (RectangleMapObject)object;
            shape = box2DShapeFactory.getRectangle(rectangle);
        }
        else if (object instanceof PolygonMapObject) {
            shape = box2DShapeFactory.getPolygon((PolygonMapObject) object);
        }
        else if (object instanceof PolylineMapObject) {
            shape = box2DShapeFactory.getPolyline((PolylineMapObject) object);
        }
        else if (object instanceof CircleMapObject) {
            shape = box2DShapeFactory.getCircle((CircleMapObject) object);
        }
        else {
            logger.error("non suported shape " + object);
        }
        return shape;
    }

    /**
     * Destroys every static body that has been created using the manager.
     */
    //TODO: Move to GameWorld?
//    @Override
//    public void destroyPhysics() {
//        for (Body body : bodies) {
//            director.getWorld().destroyBody(body);
//        }
//        bodies.clear();
//    }

    //TODO: Move this into a Materials Class and have a MaterialsCache
    private void loadMaterialsFile(FileHandle materialsFile) {
        logger.info("adding default material");
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 1.0f;
        fixtureDef.friction = 1.0f;
        fixtureDef.restitution = 0.0f;
        materials.put("default", fixtureDef);
        logger.info("loading materials file");
        try {
            JsonReader reader = new JsonReader();
            JsonValue root = reader.parse(materialsFile);
            JsonIterator materialIt = root.iterator();
            while (materialIt.hasNext()) {
                JsonValue materialValue = materialIt.next();
                if (!materialValue.has("name")) {
                    logger.error("material without name");
                    continue;
                }
                String name = materialValue.getString("name");
                fixtureDef = new FixtureDef();
                fixtureDef.density = materialValue.getFloat("density", 1.0f);
                fixtureDef.friction = materialValue.getFloat("friction", 1.0f);
                fixtureDef.restitution = materialValue.getFloat("restitution", 0.0f);
                logger.info("adding material " + name);
                materials.put(name, fixtureDef);
            }
        } catch (Exception e) {
            logger.error("error loading " + materialsFile.name() + " " + e.getMessage());
        }
    }
}
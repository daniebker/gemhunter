package lib.physics;

import com.badlogic.gdx.maps.objects.CircleMapObject;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.PolylineMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.daniebker.gamelib.TiledMapWorld;

/***
 * Class responsible for building Box2DShapes given a
 * Tiled Map Object
 */
public class Box2DShapeFactory implements ShapeFactory {

    private final float pixelsToWorld;

    public Box2DShapeFactory(TiledMapWorld tiledMapWorld) {
        this.pixelsToWorld = tiledMapWorld.getPixelsToWorld();
    }
    
    /**
     * Builds a Polygon Shape from a Map Rectangle Object
     * @param rectangleObject
     * @return: Polygon shape
     */
    @Override
    public Shape getRectangle(RectangleMapObject rectangleObject) {
        Rectangle rectangle = rectangleObject.getRectangle();
        PolygonShape polygon = new PolygonShape();
        Vector2 size = new Vector2((rectangle.x + rectangle.width * 0.5f) / pixelsToWorld,
                (rectangle.y + rectangle.height * 0.5f ) / pixelsToWorld);
        polygon.setAsBox(rectangle.width * 0.5f / pixelsToWorld,
                rectangle.height * 0.5f / pixelsToWorld,
                size,
                0.0f);
        return polygon;
    }

    /**
     * Builds a circle from a Map Circle Object
     * @param circleObject
     * @return the built circle
     */
    @Override
    public Shape getCircle(CircleMapObject circleObject) {
        Circle circle = circleObject.getCircle();
        CircleShape circleShape = new CircleShape();
        circleShape.setRadius(circle.radius / pixelsToWorld);
        circleShape.setPosition(new Vector2(circle.x / pixelsToWorld, circle.y / pixelsToWorld));
        return circleShape;
    }

    /**
     * Builds a Polygon shape given a Polygon Map Object
     * @param polygonObject
     * @return the built Polygon
     */
    @Override
    public Shape getPolygon(PolygonMapObject polygonObject) {
        PolygonShape polygon = new PolygonShape();
        float[] vertices = polygonObject.getPolygon().getTransformedVertices();
        float[] worldVertices = new float[vertices.length];
        for (int i = 0; i < vertices.length; ++i) {
            worldVertices[i] = vertices[i] / pixelsToWorld;
        }
        polygon.set(worldVertices);
        return polygon;
    }

    /**
     * Builds a Polyline given a PolylineMapObject
     * @param polylineObject
     * @return the build Polyline
     */
    @Override
    public Shape getPolyline(PolylineMapObject polylineObject) {
        float[] vertices = polylineObject.getPolyline().getTransformedVertices();
        Vector2[] worldVertices = new Vector2[vertices.length / 2];
        for (int i = 0; i < vertices.length / 2; ++i) {
            worldVertices[i] = new Vector2();
            worldVertices[i].x = vertices[i * 2] / pixelsToWorld;
            worldVertices[i].y = vertices[i * 2 + 1] / pixelsToWorld;
        }
        ChainShape chain = new ChainShape();
        chain.createChain(worldVertices);
        return chain;
    }
}
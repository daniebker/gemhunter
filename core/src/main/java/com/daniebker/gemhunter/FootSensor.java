package com.daniebker.gemhunter;

import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.utils.Array;
import com.daniebker.gemhunter.sprites.GemSprite;
import com.daniebker.gemhunter.states.StateType;
import lib.sprites.GameActor;

import java.util.HashSet;

/**
 * Created by daniel on 05.03.15.
 */
public class FootSensor implements CollisionResolver {

    private GameActor parent;

    public FootSensor(GameActor parent)
    {
        this.parent = parent;
    }

    private final Array<Fixture> contacts = new Array<Fixture>();

    @Override
    public void resolveStartCollision(Fixture fixture) {


        if(parent.getState().getStateType() != StateType.DODGE) {

            if(!(fixture.getBody().getUserData() instanceof GameActor)) {
                contacts.add(fixture);
                parent.setIsGrounded(true);
            }
        }
    }

    @Override
    public void resolveEndCollision(Fixture fixture) {

        contacts.removeValue(fixture, true);

        if(contacts.size == 0) {
            parent.setIsGrounded(false);
        }
    }
}

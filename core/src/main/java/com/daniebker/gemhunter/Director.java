package com.daniebker.gemhunter;

import com.daniebker.gemhunter.scenes.GemHunterScene;
import com.daniebker.gemhunter.scenes.MenuScene;
import com.daniebker.gemhunter.scenes.RetryScene;
import com.daniebker.gemhunter.scenes.SceneType;

/**
 * Created by daniel on 04.03.15.
 */
public class Director {

    private SceneType currentScene = SceneType.Start;
    private GemHunter game;

    public Director(GemHunter game) {

        this.game = game;
    }

    public void changeScene(SceneType newScene) {

        if(newScene != currentScene){
            currentScene = newScene;

            switch(newScene)
            {
                case Start:
                    game.setScreen(new MenuScene(game));
                    break;

                case NewGame:
                    game.setScreen(new GemHunterScene(game));
                    break;

                case Retry:
                    game.setScreen(new RetryScene(game));
                    break;
            }

        }

    }
}

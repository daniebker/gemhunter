package com.daniebker.gemhunter.layers;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.daniebker.gamelib.Observable;
import com.daniebker.gamelib.Observer;
import com.daniebker.gemhunter.Director;
import com.daniebker.gemhunter.sprites.Player;
import com.daniebker.gemhunter.definitions.AnimationCache;
import com.daniebker.gemhunter.definitions.AnimationDefinitions;
import com.daniebker.gemhunter.states.StateType;
import lib.input.InputHandler;
import lib.layers.Layer;
import lib.physics.PhysicsBodyBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dan on 29/12/14.
 */
public class PlayerLayer implements Layer, Observable {

    private Player player;

    public PlayerLayer(AssetManager assetManager,
                       InputHandler inputHandler,
                       PhysicsBodyBuilder physicsBodyBuilder,
                       Director director) {

        //TODO: Should be in the sprite class
        AnimationCache animationCache = new AnimationCache(assetManager);
        Map<StateType, Animation> animations = new HashMap<StateType, Animation>();
        animations.put(StateType.IDLE, animationCache.getAnimation(AnimationDefinitions.IDLE));
        animations.put(StateType.RUN, animationCache.getAnimation(AnimationDefinitions.RUN));
        animations.put(StateType.JUMP, animationCache.getAnimation(AnimationDefinitions.JUMP));
        animations.put(StateType.DODGE, animationCache.getAnimation(AnimationDefinitions.SLIDE));

        Sprite sprite = new Sprite();
        sprite.setPosition(1f,3f);

        //1 unit wide and 2 high
        sprite.setSize(.98f, 2f);
        BodyDef boxBodyDef = new BodyDef();
        boxBodyDef.type = BodyDef.BodyType.DynamicBody;
        boxBodyDef.position.x = sprite.getX();
        boxBodyDef.position.y = sprite.getY();
        boxBodyDef.fixedRotation = true;

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density=1f;
        fixtureDef.friction=1f;
        fixtureDef.restitution=0f;

        Body body = physicsBodyBuilder.createBodyFromSprite(sprite, boxBodyDef, fixtureDef);

        this.player = new Player(animations, sprite, body, inputHandler, director);
    }


    @Override
    public void create() {

    }

    @Override
    public void render(Batch batch) {

       player.render(batch);
    }

    @Override
    public void update(float deltaTime) {

        player.update(deltaTime);
    }

    @Override
    public void addObserver(Observer observer) {

        player.addObserver(observer);
    }

    @Override
    public boolean removeObserver(Observer observer) {

       return player.removeObserver(observer);
    }
}

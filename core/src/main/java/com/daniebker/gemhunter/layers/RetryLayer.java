package com.daniebker.gemhunter.layers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.daniebker.gemhunter.Director;
import com.daniebker.gemhunter.scenes.SceneType;
import lib.layers.Layer;

import static com.daniebker.gemhunter.definitions.AppSettings.DEV;

/**
 * Created by dan on 10/03/15.
 */
public class RetryLayer implements Layer {

    private final Director director;
    private final Stage stage;

    public RetryLayer(Director director) {
        this.director = director;
        stage = new Stage();
    }

    @Override
    public void create() {
        Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        skin.getAtlas().getTextures().iterator().next().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        skin.getFont("default-font").setMarkupEnabled(true);

        // Create a table that fills the screen. Everything else will go inside this table.
        Table table = new Table();

        // Create a gemScore with the "default" TextButtonStyle. A 3rd parameter can be used to specify a name other than "default".
        TextButton retryButton = new TextButton("Retry", skin);
        retryButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                director.changeScene(SceneType.NewGame);
            }
        });

        TextButton quitButton = new TextButton("Quit", skin);
        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                director.changeScene(SceneType.Start);
            }
        });

        table.add(retryButton);
        table.add(quitButton);
        table.setFillParent(true);

        if(DEV) {
            table.setDebug(DEV);
        }

        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void render(Batch batch) {
        stage.draw();
    }

    @Override
    public void update(float deltaTime) {
        stage.act(deltaTime);
    }
}

package com.daniebker.gemhunter.layers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.daniebker.gamelib.TiledMapWorld;
import com.daniebker.gemhunter.definitions.LevelDefinitions;
import lib.layers.Layer;
import lib.physics.Box2DMapBodyBuilder;

import static com.daniebker.gemhunter.definitions.AppSettings.DEV;

/**
 * Created by dan on 30/12/14.
 */
public class PhysicsLayer implements Layer {

    private final Box2DDebugRenderer debugRenderer;
    private TiledMapWorld tiledMapWorld;
    private Camera camera;

    public PhysicsLayer(TiledMapWorld tiledMapWorld,
                        OrthographicCamera camera,
                        Box2DMapBodyBuilder box2DMapBodyBuilder) {

        this.tiledMapWorld = tiledMapWorld;
        this.camera = camera;

        debugRenderer = new Box2DDebugRenderer();

        TiledMap map = tiledMapWorld.getWorldMap();

        box2DMapBodyBuilder.setUpMaterials(Gdx.files.internal("data/materials.json"));
        box2DMapBodyBuilder.createPhysics(map, LevelDefinitions.PHYSICS_LAYER);
    }

    @Override
    public void create() {

    }

    @Override
    public void render(Batch batch) {
        if(DEV) {
            debugRenderer.render(tiledMapWorld.getWorld(), camera.combined);
        }
    }

    @Override
    public void update(float deltaTime) {
        if(!tiledMapWorld.getWorld().isLocked()) {
            tiledMapWorld.getWorld().step(deltaTime, 6, 2);
        }
    }
}

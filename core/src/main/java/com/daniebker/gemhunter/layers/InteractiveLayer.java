package com.daniebker.gemhunter.layers;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.utils.Array;
import com.daniebker.gemhunter.sprites.GemSprite;
import com.daniebker.gemhunter.sprites.SpikeSprite;
import com.daniebker.gemhunter.sprites.SpriteBuilder;
import lib.sprites.GameActor;
import lib.layers.Layer;

/**
 * Created by dan on 15/01/15.
 */
public class InteractiveLayer implements Layer {

    private Array<GameActor> sprites;
    private SpriteBuilder spriteBuilder;

    public InteractiveLayer(SpriteBuilder spriteBuilder) {
        this.spriteBuilder = spriteBuilder;
        this.sprites = new Array<GameActor>();
    }

    @Override
    public void create() {
        sprites.addAll(spriteBuilder.buildSprites(GemSprite.NAME));
        sprites.addAll(spriteBuilder.buildSprites(SpikeSprite.NAME));
    }

    @Override
    public void render(Batch batch) {
        for (GameActor sprite : sprites) {
            sprite.render(batch);
        }
    }

    @Override
    public void update(float deltaTime) {

        for (GameActor sprite : sprites) {
            if(sprite.getIsDestroyed()) {
                sprites.removeValue(sprite, false);
            }

            sprite.update(deltaTime);
        }
    }
}

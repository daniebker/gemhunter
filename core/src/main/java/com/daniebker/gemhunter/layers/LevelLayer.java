package com.daniebker.gemhunter.layers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.daniebker.gamelib.TiledMapWorld;
import lib.layers.Layer;


/**
 * Created by dan on 30/12/14.
 */
public class LevelLayer implements Layer {

    private final OrthographicCamera camera;
    private OrthogonalTiledMapRenderer renderer;

    public LevelLayer(Batch batch,
                      OrthographicCamera camera,
                      TiledMapWorld tiledMapWorld) {

        this.camera = camera;

        TiledMap map = tiledMapWorld.getWorldMap();
        renderer =  new OrthogonalTiledMapRenderer(map, 1 / tiledMapWorld.getPixelsToWorld(), batch);
    }

    @Override
    public void create() {

    }

    @Override
    public void render(Batch batch) {

        batch.end();
        renderer.render();
        batch.begin();
    }

    @Override
    public void update(float deltaTime) {

        renderer.setView(camera);}
}

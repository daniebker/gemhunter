package com.daniebker.gemhunter.layers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.daniebker.gemhunter.Director;
import com.daniebker.gemhunter.scenes.SceneType;
import lib.layers.Layer;

import static com.daniebker.gemhunter.definitions.AppSettings.DEV;

/**
 * Created by dan on 25/02/15.
 *
 * TODO: Button click transports to start game
 */
public class MenuLayer implements Layer {

    private final Stage stage;
    private final TextButton gemScore;
    private final Director director;

    public MenuLayer(final Director director) {
        this.director = director;
        stage = new Stage();

        Skin skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        skin.getAtlas().getTextures().iterator().next().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        skin.getFont("default-font").setMarkupEnabled(true);

        // Create a table that fills the screen. Everything else will go inside this table.
        Table table = new Table();

        // Create a gemScore with the "default" TextButtonStyle. A 3rd parameter can be used to specify a name other than "default".
        gemScore = new TextButton("Start Game", skin);

        gemScore.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y)
            {
                director.changeScene(SceneType.NewGame);
            }});

        table.add(gemScore);

        table.setFillParent(true);
        if(DEV) {
            table.setDebug(true);
        }

        stage.addActor(table);

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void create() {

    }

    @Override
    public void render(Batch batch) {

        stage.draw();
    }

    @Override
    public void update(float deltaTime) {

        stage.act(deltaTime);
    }
}

package com.daniebker.gemhunter.layers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.daniebker.gemhunter.GameState;
import lib.layers.Layer;

import static com.daniebker.gemhunter.definitions.AppSettings.DEV;

/**
 * Created by dan on 24/02/15.
 */
public class StatsLayer implements Layer {

    private float playTime = .0f;

    private final Label gemScoreLabel;
    private final Label timerLabel;
    private Stage stage;
    private Skin skin;

    public StatsLayer() {

        stage = new Stage();

        skin = new Skin(Gdx.files.internal("data/uiskin.json"));
        skin.getAtlas().getTextures().iterator().next().setFilter(Texture.TextureFilter.Nearest, Texture.TextureFilter.Nearest);
        skin.getFont("default-font").setMarkupEnabled(true);

        Table table = new Table();

        timerLabel = new Label("", skin);
        gemScoreLabel = new Label("", skin);

        table.add(timerLabel);
        table.add(gemScoreLabel);

        table.setFillParent(true);
        table.left();
        table.top();

        if(DEV) {
            table.setDebug(true);
        }

        stage.addActor(table);
    }

    @Override
    public void create() {

    }

    @Override
    public void render (Batch batch) {

        stage.draw();
    }

    @Override
    public void update(float deltaTime) {
        playTime += deltaTime;
        gemScoreLabel.setText("Gems: " + GameState.getScore());
        timerLabel.setText("Time:    ");
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
    }

    //@Override
    public void resize (int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    //@Override
    public void dispose () {
        stage.dispose();
        skin.dispose();
    }
}

package com.daniebker.gemhunter;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.daniebker.gemhunter.scenes.GemHunterScene;
import lib.AssetLoader;
import lib.GameManager;

public class GemHunter extends Game implements GameManager {

	private final AssetManager assetManager;
	private final FPSLogger fpsLogger;
    private final AssetLoader assetLoader;
    private Director director;
	private SpriteBatch batch;

	//Getters and Setters
    @Override
	public SpriteBatch getBatch() {
		return batch;
	}

    @Override
	public AssetLoader getAssetLoader() {
		return assetLoader;
	}

    @Override
    public Director getDirector() {
        return director;
    }

	public GemHunter() {

        //TODO: USe Guice
		assetManager = new AssetManager();
		fpsLogger = new FPSLogger();
		director = new Director(this);
        assetLoader = new AssetLoader(assetManager);
	}

	@Override
	public void create () {

		batch = new SpriteBatch();

        assetLoader.loadAssets();

		setScreen(new GemHunterScene(this));
	}

	@Override
	public void render() {

		fpsLogger.log();

		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		super.render();
	}

	@Override
	public void resize (int width, int height)
	{
		//viewport.update(width, height);
	}

	@Override
	public void dispose() {
		assetManager.dispose();
		batch.dispose();
	}
}

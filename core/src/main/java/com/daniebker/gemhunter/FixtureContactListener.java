package com.daniebker.gemhunter;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.badlogic.gdx.utils.Logger;

/**
 * Created by dan on 14/12/14.
 */

public class FixtureContactListener implements ContactListener {

    @Override
    public void beginContact(Contact contact) {

        Object bodyUserData = contact.getFixtureA().getBody().getUserData();
        if (bodyUserData instanceof CollisionResolver) {
            CollisionResolver entity = (CollisionResolver)bodyUserData;

            entity.resolveStartCollision(contact.getFixtureB());
        }

        bodyUserData = contact.getFixtureB().getBody().getUserData();
        if (bodyUserData instanceof CollisionResolver) {
            CollisionResolver entity = (CollisionResolver)bodyUserData;

            entity.resolveStartCollision(contact.getFixtureA());
        }

        Object fixtureUserData = contact.getFixtureA().getUserData();
        if (fixtureUserData instanceof CollisionResolver) {
            CollisionResolver entity = (CollisionResolver)fixtureUserData;

            entity.resolveStartCollision(contact.getFixtureB());
        }

        fixtureUserData = contact.getFixtureB().getUserData();
        if (fixtureUserData instanceof CollisionResolver) {
            CollisionResolver entity = (CollisionResolver)fixtureUserData;

            entity.resolveStartCollision(contact.getFixtureA());
        }
    }

    @Override
    public void endContact(Contact contact)  {

        if(!contact.isTouching()) {

            Object bodyUserData = contact.getFixtureA().getBody().getUserData();
            if (bodyUserData instanceof CollisionResolver) {
                CollisionResolver entity = (CollisionResolver) bodyUserData;

                entity.resolveEndCollision(contact.getFixtureB());
            }

            bodyUserData = contact.getFixtureB().getBody().getUserData();
            if (bodyUserData instanceof CollisionResolver) {
                CollisionResolver entity = (CollisionResolver) bodyUserData;

                entity.resolveEndCollision(contact.getFixtureA());
            }

            Object fixtureUserData = contact.getFixtureA().getUserData();
            if (fixtureUserData instanceof CollisionResolver) {
                CollisionResolver entity = (CollisionResolver) fixtureUserData;

                entity.resolveEndCollision(contact.getFixtureB());
            }

            fixtureUserData = contact.getFixtureB().getUserData();
            if (fixtureUserData instanceof CollisionResolver) {
                CollisionResolver entity = (CollisionResolver) fixtureUserData;

                entity.resolveEndCollision(contact.getFixtureA());
            }
        }
    }


    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

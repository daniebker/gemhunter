package com.daniebker.gemhunter;

import com.badlogic.gdx.physics.box2d.Fixture;
import com.daniebker.gemhunter.states.StateType;
import lib.sprites.Direction;
import lib.sprites.GameActor;

/**
 * Created by Daniel on 22/03/2015.
 */
public class LeftSideSensor implements CollisionResolver {

    private GameActor parent;

    public LeftSideSensor(GameActor parent)
    {
        this.parent = parent;
    }

    @Override
    public void resolveStartCollision(Fixture fixture) {

        if(parent.getState().getStateType() == StateType.DODGE
        && !parent.getFacesRight()) {

            parent.setCanStand(false);
        }

        if(!parent.getIsGrounded()
        && !(fixture.getUserData() instanceof GameActor)) {
            parent.setDirection(Direction.STOPPED);
        }
    }

    @Override
    public void resolveEndCollision(Fixture fixture) {

        if(parent.getState().getStateType() == StateType.DODGE
        && !parent.getFacesRight()
        && !parent.getCanStand()) {

            parent.setCanStand(true);
        }
    }
}
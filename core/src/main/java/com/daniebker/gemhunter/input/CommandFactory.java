package com.daniebker.gemhunter.input;

import lib.input.Command;

/**
 * CommandFactory
 * Builds commands based on the names of
 * the command.
 *
 * dan - 19/03/15
 */
public class CommandFactory {

    /**
     * Gets a starting command.
     *
     * @param commandName The name of the command to build
     * @return Returns the command given the name. If the
     * command does not exist returns null
     */
    public Command getStartCommand(String commandName) {

        if(commandName == null) {
            return null;
        }

        if (commandName.equals(MoveLeftCommand.NAME)) {
            return new MoveLeftCommand();
        } else if (commandName.equals(MoveRightCommand.NAME)) {
            return new MoveRightCommand();
        } else if (commandName.equals(JumpCommand.NAME)) {
            return new JumpCommand();
        } else if (commandName.equals(DodgeCommand.NAME)) {
            return new DodgeCommand();
        } else {
            return null;
        }
    }

    /**
     * Gets an Ending Command
     *
     * @param commandName the Command name
     * @return The command associated with the given name.
     * If no command is found returns null.
     */
    public Command getEndCommand(String commandName) {

        if(commandName == null) {
            return null;
        }

        if (commandName.equals(MoveLeftCommand.NAME)) {
            return new StopRunningLeftCommand();
        } else if (commandName.equals(MoveRightCommand.NAME)) {
            return new StopRunningRightCommand();
        }
        else if (commandName.equals(JumpCommand.NAME)) {
            return new StopJumpingCommand();
        } else if (commandName.equals(DodgeCommand.NAME)) {
            return new StopDodgingCommand();
        } else {
            return null;
        }
    }
}

package com.daniebker.gemhunter.input;

import com.daniebker.gemhunter.states.DodgeState;
import com.daniebker.gemhunter.states.StateType;
import lib.input.Command;
import lib.sprites.GameActor;

/**
 * Created by dan on 7/03/15.
 */
public class DodgeCommand implements Command {

    public static final String NAME = "DodgeCommand";
    private boolean handled;

    @Override
    public void execute(GameActor gameActor) {

        handled = true;

        if(gameActor.getIsGrounded()
           && gameActor.getState().getStateType() != StateType.IDLE) {
            gameActor.changeState(new DodgeState());
        }
    }

    @Override
    public boolean wasHandled() {
        return handled;
    }
}

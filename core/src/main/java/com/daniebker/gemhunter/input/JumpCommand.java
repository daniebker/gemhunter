package com.daniebker.gemhunter.input;

import com.daniebker.gemhunter.states.JumpState;
import lib.input.Command;
import lib.sprites.GameActor;

/**
 * Created by dan on 29/12/14.
 */
public class JumpCommand implements Command {

    public static final String NAME = "JumpCommand";
    private boolean handled = false;

    @Override
    public void execute(GameActor gameActor) {

        handled = true;

        if(gameActor.getIsGrounded()) {

            gameActor.changeState(new JumpState());
        }
    }

    @Override
    public boolean wasHandled() {
        return handled;
    }
}

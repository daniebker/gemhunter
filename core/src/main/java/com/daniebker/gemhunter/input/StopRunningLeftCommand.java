package com.daniebker.gemhunter.input;

import lib.input.Command;
import lib.sprites.Direction;
import lib.sprites.GameActor;

/**
 * Created by dan on 17/03/15.
 */
public class StopRunningLeftCommand implements Command {

    private boolean handled;

    @Override
    public void execute(GameActor gameActor) {

        if(gameActor.getDirection() == Direction.LEFT) {
            gameActor.setDirection(Direction.STOPPED);
        }

        handled = true;
    }

    @Override
    public boolean wasHandled() {
        return handled;
    }
}

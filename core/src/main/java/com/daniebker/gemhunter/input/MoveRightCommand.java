package com.daniebker.gemhunter.input;

import com.daniebker.gemhunter.states.DodgeState;
import com.daniebker.gemhunter.states.RunState;
import lib.input.Command;
import lib.sprites.Direction;
import lib.sprites.GameActor;

/**
 * Created by dan on 29/12/14.
 */
public class MoveRightCommand implements Command {

    public static final String NAME = "MoveRightCommand";
    private boolean handled = false;

    @Override
    public void execute(GameActor gameActor) {

        gameActor.setFacesRight(true);
        gameActor.setDirection(Direction.RIGHT);

        if(!(gameActor.getState() instanceof DodgeState)
        && gameActor.getIsGrounded()) {

            gameActor.changeState(new RunState());
        }
        handled = true;
    }

    @Override
    public boolean wasHandled() {
        return handled;
    }
}

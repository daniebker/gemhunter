package com.daniebker.gemhunter.input;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.Array;
import lib.input.Command;
import lib.input.InputHandler;

/**
 * Processes input for a game.
 *
 * dan - 10/12/14.
 */
public class GameInputHandler implements InputHandler, InputProcessor {

    private final Array<Command> commands = new Array();
    private ButtonMapping buttonMapping;

    public GameInputHandler(ButtonMapping buttonMapping) {

        this.buttonMapping = buttonMapping;
    }

    @Override
    public Array<Command> getCommands() {

        for(Command command : commands){
            if(command.wasHandled()) {
                commands.removeValue(command, true);
            }
        }

        return commands;
    }

    @Override
    public boolean keyDown(int keycode) {

        Command command = buttonMapping.getStartCommand(keycode);
        if(command != null) {
            commands.add(command);
            return true;
        }

            return false;
    }

    @Override
    public boolean keyUp(int keycode) {

        Command command = buttonMapping.getEndCommand(keycode);
        if(command != null) {
            commands.add(command);
            return true;
        }

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
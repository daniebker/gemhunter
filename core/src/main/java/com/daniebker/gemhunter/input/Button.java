package com.daniebker.gemhunter.input;

import lib.input.Command;

/**
 * Represents a button mapping to a command
 *
 * dan - 19/03/15.
 */
public class Button {

    private int keyCode;
    private Command command;

    public Button(int keyCode, Command command) {

        this.keyCode = keyCode;
        this.command = command;
    }

    public int getKeyCode() {
        return keyCode;
    }

    public void setKeyCod(int value) {
        keyCode = value;
    }

    public Command getCommand() {
        return command;
    }
}

package com.daniebker.gemhunter.input;

import com.badlogic.gdx.Input;
import lib.input.Command;

import java.util.HashMap;

/**
 * ButtonMapping Maps a button
 * to a command
 *
 * dan - 19/03/15
 */
public class ButtonMapping {

    private HashMap<Integer, String> keyMappings;
    private CommandFactory commandFactory;

    public ButtonMapping(CommandFactory commandFactory) {
        this.commandFactory = commandFactory;

        keyMappings = new HashMap<Integer, String>();
    }

    public void buildDefaultMappings() {

        keyMappings.put(Input.Keys.LEFT, MoveLeftCommand.NAME);
        keyMappings.put(Input.Keys.RIGHT, MoveRightCommand.NAME);
        keyMappings.put(Input.Keys.SPACE, JumpCommand.NAME);
        keyMappings.put(Input.Keys.Z, DodgeCommand.NAME);
    }

    public Command getStartCommand(int keyCode) {

        return commandFactory.getStartCommand(keyMappings.get(keyCode));
    }

    public Command getEndCommand(int keyCode) {

        return commandFactory.getEndCommand(keyMappings.get(keyCode));
    }
}

package com.daniebker.gemhunter.input;

import com.daniebker.gemhunter.states.StateType;
import lib.input.Command;
import lib.sprites.GameActor;

/**
 * Created by Daniel on 26/03/2015.
 */
public class StopDodgingCommand implements Command {

    private boolean wasHandled;

    @Override
    public void execute(GameActor gameActor) {
        wasHandled = true;
        if(gameActor.getState().getStateType() == StateType.DODGE) {
            gameActor.setDodgeEnd(true);
        }
    }

    @Override
    public boolean wasHandled() {
        return wasHandled;
    }
}

package com.daniebker.gemhunter.input;

import lib.input.Command;
import lib.sprites.GameActor;

/**
 * Created by dan on 17/03/15.
 */
public class StopJumpingCommand implements Command {

    private boolean handled;

    @Override
    public void execute(GameActor gameActor) {

            handled = true;
            gameActor.setJumpHasPeaked(true);
    }

    @Override
    public boolean wasHandled() {
        return handled;
    }
}

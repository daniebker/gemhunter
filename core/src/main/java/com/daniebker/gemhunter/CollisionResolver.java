package com.daniebker.gemhunter;

import com.badlogic.gdx.physics.box2d.Fixture;

/**
 * Created by daniel on 05.03.15.
 */
public interface CollisionResolver {
    void resolveStartCollision(Fixture fixture);

    void resolveEndCollision(Fixture fixture);
}

package com.daniebker.gemhunter.states;

/**
 * Created by dan on 29/12/14.
 */
public enum StateType {
    RUN,
    IDLE,
    DODGE,
    STOPPING,
    JUMP
}

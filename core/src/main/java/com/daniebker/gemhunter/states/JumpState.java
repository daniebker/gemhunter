package com.daniebker.gemhunter.states;

import lib.sprites.GameActor;
import lib.states.State;

/**
 * Created by dan on 29/12/14.
 */
public class JumpState extends State {

    public JumpState() {

        super(StateType.JUMP);
    }

    @Override
    public void enter(GameActor gameActor) {
        super.enter(gameActor);

        gameActor.setIsGrounded(false);
        gameActor.setJumpHasPeaked(false);
        gameActor.setCurrentAnimationByState(getStateType());
    }

    @Override
    public void update(float deltaTime, GameActor gameActor) {

        super.update(deltaTime, gameActor);
        float force = 0f;

        if(!gameActor.getJumpHasPeaked()) {

            force = gameActor.getForceVector().y;
        }

        if(getStateTime() > 0.09f ) {
            gameActor.setJumpHasPeaked(true);
        }

        if(gameActor.getJumpHasPeaked()) {
            gameActor.changeState(new StoppingState());
        }

        //TODO: Physics Handler
        gameActor.getBody().applyLinearImpulse(0f, force, gameActor.getX(), gameActor.getY(), true);
    }

    @Override
    public void exit(GameActor gameActor) {
        super.exit(gameActor);

        gameActor.setJumpHasPeaked(false);
    }
}

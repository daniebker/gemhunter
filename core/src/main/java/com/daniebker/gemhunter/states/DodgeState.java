package com.daniebker.gemhunter.states;

import lib.sprites.GameActor;
import lib.states.State;

/**
 * Represents the player dodging.
 * Player should slide along the surface under their own power
 * Player cannot slide if they're idle.
 */
public class DodgeState extends State {

    //TODO: Shoould be part of player
    private static final float MINIMUM_VALOCITY_X = .5f;

    public DodgeState(){

        super(StateType.DODGE);
    }

    @Override
    public void enter(GameActor gameActor) {

        if(!gameActorIsStopping(gameActor)) {
            super.enter(gameActor);
            if (gameActor.getIsGrounded()) {
                gameActor.setCurrentAnimationByState(getStateType());
                if (gameActor.getFacesRight()) {
                    gameActor.getBody().setTransform(gameActor.getX(), gameActor.getY(), getDodgeAngle());
                    gameActor.getBody().applyForceToCenter(1500f, 0f, true);

                } else {
                    gameActor.getBody().setTransform(gameActor.getX(), gameActor.getY(), -getDodgeAngle());
                    gameActor.getBody().applyForceToCenter(-1500f, 0f, true);
                }
            }
        }
    }

    @Override
    public void update(float deltaTime, GameActor gameActor) {

        super.update(deltaTime, gameActor);

        if(gameActorIsStopping(gameActor) && gameActor.getCanStand()) {

            gameActor.getBody().setTransform(gameActor.getX() + gameActor.getWidth()/2f, gameActor.getY() + gameActor.getHeight()/2f, 0f);
            gameActor.changeState(new StoppingState());
        }

        if(gameActor.getDodgeEnd() && gameActor.getCanStand()) {

            gameActor.getBody().setTransform(gameActor.getX() + gameActor.getWidth()/2f, gameActor.getY() + gameActor.getHeight()/2f, 0f);
            gameActor.changeState(new StoppingState());
        }
    }

    @Override
    public void exit(GameActor gameActor) {
        super.exit(gameActor);
        gameActor.setDodgeEnd(false);
    }

    private boolean gameActorIsStopping(GameActor gameActor) {
        return gameActor.getVelocity().x < MINIMUM_VALOCITY_X && gameActor.getVelocity().x > -MINIMUM_VALOCITY_X;
    }

    private float getDodgeAngle() {
        return (float) (90f * (Math.PI / 180f));
    }
}

package com.daniebker.gemhunter.states;

import lib.sprites.Direction;
import lib.sprites.GameActor;
import lib.states.State;

/**
 * Created by dan on 29/12/14.
 */
public class IdleState extends State {

    public IdleState() {

        super(StateType.IDLE);
    }

    @Override
    public void enter(GameActor gameActor) {
        super.enter(gameActor);
        gameActor.setCurrentAnimationByState(getStateType());
        gameActor.setDirection(Direction.STOPPED);
    }

    @Override
    public void update(float deltaTime, GameActor gameActor) {
        super.update(deltaTime, gameActor);
    }

    @Override
    public void exit(GameActor gameActor) {
        super.exit(gameActor);
    }
}

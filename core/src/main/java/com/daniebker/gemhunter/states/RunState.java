package com.daniebker.gemhunter.states;

import lib.sprites.Direction;
import lib.sprites.GameActor;
import lib.states.State;

/**
 * Created by dan on 29/12/14.
 */
public class RunState extends State {

    public RunState(){

        super(StateType.RUN);
    }

    @Override
    public void enter(GameActor gameActor) {
        super.enter(gameActor);
        gameActor.setCurrentAnimationByState(getStateType());
    }

    @Override
    public void update(float deltaTime, GameActor gameActor) {

        super.update(deltaTime, gameActor);

        if(gameActor.getDirection() == Direction.STOPPED) {
            gameActor.changeState(new StoppingState());
        }

    }

    @Override
    public void exit(GameActor gameActor) {
        super.exit(gameActor);
    }
}

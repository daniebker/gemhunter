package com.daniebker.gemhunter.states;

import lib.sprites.Direction;
import lib.sprites.GameActor;
import lib.states.State;

/**
 * State to handle when the GameActor
 * stops performing an Action.
 */
public class StoppingState extends State {

    public StoppingState() {

        super(StateType.STOPPING);
    }

    @Override
    public void enter(GameActor gameActor) {
        super.enter(gameActor);
    }

    @Override
    public void update(float deltaTime, GameActor gameActor) {
        super.update(deltaTime, gameActor);

        if(gameActor.getIsGrounded()) {

            if(gameActor.getDirection() == Direction.STOPPED) {
                gameActor.changeState(new IdleState());
            }
            else{
                gameActor.changeState(new RunState());
            }

        }
    }

    @Override
    public void exit(GameActor gameActor) {
        super.exit(gameActor);
    }

    @Override
    public StateType getStateType() {
        return StateType.STOPPING;
    }
}

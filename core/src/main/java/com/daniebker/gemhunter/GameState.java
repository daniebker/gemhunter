package com.daniebker.gemhunter;

/**
 * Created by dan on 23/02/15.
 */
public class GameState {

    private static int score;

    public static int getScore() {
        return score;
    }

    public static void increaseScore() {
        ++score;
    }
}

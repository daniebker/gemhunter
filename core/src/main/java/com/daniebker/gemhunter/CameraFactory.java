package com.daniebker.gemhunter;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.daniebker.gamelib.cameras.DebuggableCamera;
import com.daniebker.gamelib.cameras.ObservableGameCamera;
import lib.FollowCamera;

/**
 * CameraFactory
 * dan - 9/04/15
 */
public class CameraFactory {

    public CameraFactory() {
    }

    public ObservableGameCamera getFollowCamera(float worldWidth, float worldHeight, float pixelsToWorld) {

        OrthographicCamera orthographicCamera = new OrthographicCamera(
                Gdx.graphics.getWidth()/pixelsToWorld,
                Gdx.graphics.getHeight()/pixelsToWorld);

        ObservableGameCamera camera = new FollowCamera(orthographicCamera, worldWidth, worldHeight);

        return camera;
    }

    public ObservableGameCamera getDebuggableFollowCamera(float worldWidth, float worldHeight, float pixelsToWorld){
        return new DebuggableCamera(getFollowCamera(worldWidth, worldHeight, pixelsToWorld));
    }
}

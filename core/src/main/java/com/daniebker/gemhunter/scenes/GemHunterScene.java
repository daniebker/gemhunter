package com.daniebker.gemhunter.scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.daniebker.gemhunter.CameraFactory;
import lib.sprites.GameActor;
import com.daniebker.gamelib.TiledMapWorld;
import com.daniebker.gamelib.cameras.ObservableGameCamera;
import com.daniebker.gemhunter.FixtureContactListener;
import com.daniebker.gemhunter.definitions.AppSettings;
import com.daniebker.gemhunter.definitions.LevelDefinitions;
import com.daniebker.gemhunter.input.ButtonMapping;
import com.daniebker.gemhunter.input.CommandFactory;
import com.daniebker.gemhunter.input.GameInputHandler;
import com.daniebker.gemhunter.layers.*;
import com.daniebker.gemhunter.sprites.SpriteBuilder;
import com.daniebker.gemhunter.sprites.SpriteFactory;
import lib.AssetLoader;
import lib.GameManager;
import lib.input.InputHandler;
import lib.layers.Layer;
import lib.physics.Box2DBodyBuilder;
import lib.physics.Box2DMapBodyBuilder;
import lib.physics.Box2DShapeFactory;
import lib.physics.PhysicsBodyBuilder;
import lib.scenes.Scene;

import static com.daniebker.gemhunter.definitions.AppSettings.DEV;
/**
 * Created by dan on 29/12/14.
 */
public class GemHunterScene extends Scene {

    private final AssetManager assetManager;
    private final ObservableGameCamera camera;
    private final World world;

    private final PlayerLayer playerLayer;
    private final Layer levelLayer;
    private final Layer physicsLayer;
    private final Layer gemsLayer;
    private final Layer statsLayer;

    public GemHunterScene(GameManager game) {
        super(game);

        AssetLoader assetBuilder = game.getAssetLoader();
        assetBuilder.loadAssets();

        this.assetManager = game.getAssetLoader().getAssetManager();

        //TODO: Refactor Level Selection
        TiledMap map = assetManager.get(LevelDefinitions.LEVEL3);
        TiledMapWorld tiledMapWorld = new TiledMapWorld(map);

        //TODO:Move input setup
        InputMultiplexer inputMultiplexer = new InputMultiplexer();
        ButtonMapping buttonMapping = new ButtonMapping(new CommandFactory());
        buttonMapping.buildDefaultMappings();
        InputProcessor inputHandler = new GameInputHandler(buttonMapping);
        inputMultiplexer.addProcessor(inputHandler);

        CameraFactory cameraFactory = new CameraFactory();

        if (DEV) {
            camera = cameraFactory.getDebuggableFollowCamera(tiledMapWorld.getWorldWidth(), tiledMapWorld.getWorldHeight(), tiledMapWorld.getPixelsToWorld());
            inputMultiplexer.addProcessor((InputProcessor)camera);
        }
        else {
            camera = cameraFactory.getFollowCamera(tiledMapWorld.getWorldWidth(), tiledMapWorld.getWorldHeight(), tiledMapWorld.getPixelsToWorld());
        }

        Gdx.input.setInputProcessor(inputMultiplexer);

        this.world = tiledMapWorld.getWorld();

        world.setContactListener(new FixtureContactListener());

        //Add a physics Layer
        Box2DMapBodyBuilder box2DMapBodyBuilder =
                new Box2DMapBodyBuilder(new Box2DShapeFactory(tiledMapWorld), new Box2DBodyBuilder(world));
        PhysicsBodyBuilder physicsBodyBuilder = new PhysicsBodyBuilder(world);
        physicsLayer = new PhysicsLayer(tiledMapWorld, camera.getOrthographicCamera(), box2DMapBodyBuilder);
        addLayer(physicsLayer);

        //Load the level
        levelLayer = new LevelLayer(batch, camera.getOrthographicCamera(), tiledMapWorld);
        addLayer(levelLayer);

        //gems
        SpriteBuilder spriteBuilder = new SpriteBuilder(tiledMapWorld, physicsBodyBuilder, new SpriteFactory());
        gemsLayer = new InteractiveLayer(spriteBuilder);
        addLayer(gemsLayer);

        //Add the player
        playerLayer = new PlayerLayer(assetManager, (InputHandler)inputHandler, physicsBodyBuilder, game.getDirector());
        playerLayer.addObserver(camera);
        addLayer(playerLayer);

        //UiLayer
        statsLayer = new StatsLayer();
        addLayer(statsLayer);
    }

    @Override
    public void render(float delta) {
        camera.getOrthographicCamera().update();
        batch.setProjectionMatrix(camera.getOrthographicCamera().combined);

        super.render(delta);

        if (AppSettings.DEV) {
            physicsLayer.render(batch);
        }

        Array<Body> bodies = new Array<Body>();
        world.getBodies(bodies);

        for (Body body : bodies) {
            if (body.getUserData() instanceof GameActor) {
                GameActor data = (GameActor) body.getUserData();
                if (data.getIsDestroyed()) {
                    world.destroyBody(body);
                    body.setUserData(null);
                    body = null;
                }
            }
        }
    }
}

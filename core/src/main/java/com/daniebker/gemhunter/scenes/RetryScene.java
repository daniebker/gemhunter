package com.daniebker.gemhunter.scenes;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.daniebker.gemhunter.GemHunter;
import com.daniebker.gemhunter.layers.RetryLayer;

/**
 * Created by dan on 10/03/15.
 */
public class RetryScene extends ScreenAdapter {
    private final RetryLayer retryLayer;
    private final SpriteBatch batch;

    public RetryScene(GemHunter gemHunter) {

        this.batch = gemHunter.getBatch();

        retryLayer = new RetryLayer(gemHunter.getDirector());
        retryLayer.create();
    }

    @Override
    public void render(float delta) {
        retryLayer.render(batch);
    }
}

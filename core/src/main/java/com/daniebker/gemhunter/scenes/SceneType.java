package com.daniebker.gemhunter.scenes;

/**
 * Created by daniel on 04.03.15.
 */
public enum SceneType {
    Start, NewGame, Retry
}

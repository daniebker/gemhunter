package com.daniebker.gemhunter.scenes;

import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.daniebker.gemhunter.GemHunter;
import com.daniebker.gemhunter.layers.MenuLayer;

/**
 * Created by dan on 25/02/15.
 */
public class MenuScene  extends ScreenAdapter {


    private final MenuLayer menuLayer;
    private final SpriteBatch batch;

    public MenuScene(GemHunter gemHunter) {

        this.batch = gemHunter.getBatch();

        menuLayer = new MenuLayer(gemHunter.getDirector());
    }

    @Override
    public void render(float delta) {
        menuLayer.render(batch);
    }

}

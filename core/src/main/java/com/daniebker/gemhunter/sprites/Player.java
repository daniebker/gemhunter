package com.daniebker.gemhunter.sprites;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Logger;
import com.badlogic.gdx.utils.TimeUtils;
import com.daniebker.gemhunter.*;
import com.daniebker.gemhunter.definitions.AppSettings;
import com.daniebker.gemhunter.scenes.SceneType;
import com.daniebker.gemhunter.states.IdleState;
import com.daniebker.gemhunter.states.StateType;
import lib.input.Command;
import lib.input.InputHandler;
import com.daniebker.gamelib.Observable;
import com.daniebker.gamelib.Observer;
import lib.sprites.Box2DSprite;
import lib.sprites.Direction;

import java.util.HashSet;
import java.util.Map;

import static com.daniebker.gemhunter.definitions.AppSettings.DEV;


/**
 * Created by dan on 2/01/15.
 */
public class Player extends Box2DSprite implements Observable {

    private static final float MOVEMENT_DAMPENING = 0.15f;
    //Animations
    private Map<StateType, Animation> animations;

    private final Logger logger;
    private final Director director;

    //Context
    private float maxVelocity = 14f;
    private Vector2 acceleration = new Vector2(200f, 5f);
    private HashSet<Observer> observers = new HashSet<Observer>();
    private InputHandler inputHandler;
    private long lastTime;
    private StateType currentAnimation;

    //Getters Setters
    @Override
    public float getMaxVelocity() { return maxVelocity; }

    @Override
    public Vector2 getForceVector() { return acceleration; }

    public Player(Map<StateType, Animation> animations,
                  Sprite sprite,
                  Body body,
                  InputHandler inputHandler,
                  Director director) {
        super(body, sprite);

        this.inputHandler = inputHandler;
        this.logger = new Logger(this.getClass().getSimpleName() + "Logger", AppSettings.LOGGING_LEVEL);
        this.director = director;

        this.animations = animations;
        this.state = new IdleState();
        this.currentAnimation = StateType.IDLE;

        FixtureDef fixtureDef = new FixtureDef();
        PolygonShape shape = new PolygonShape();

        //FOOT SENSOR
        shape.setAsBox(sprite.getWidth()/2f-.05f, sprite.getHeight() / 25f, new Vector2(0f, -1.f), 0);
        fixtureDef.isSensor = true;
        fixtureDef.shape = shape;
        Fixture footSensor = body.createFixture(fixtureDef);
        footSensor.setUserData(new FootSensor(this));

        //LEFT SENSOR
        shape.setAsBox(sprite.getWidth()/20f, (sprite.getHeight()/2f) - .1f, new Vector2(-.5f,0), 0);
        fixtureDef.shape = shape;
        Fixture leftSensor = body.createFixture(fixtureDef);
        leftSensor.setUserData(new LeftSideSensor(this));

        //RIGHT SENSOR
        shape.setAsBox(sprite.getWidth()/20f, (sprite.getHeight()/2f) - .1f, new Vector2(.5f,0), 0);
        fixtureDef.shape = shape;
        Fixture rightSensor = body.createFixture(fixtureDef);
        rightSensor.setUserData(new RightSideSensor(this));
        rightSensor.setFriction(1f);

        body.setUserData(this);
    }

    @Override
    public void destroy() {
        super.destroy();
        director.changeScene(SceneType.Retry);
    }

    @Override
    public void render(Batch batch) {
        super.render(batch);
    }

    @Override
    public void update(float deltaTime) {

        super.update(deltaTime);

        state.update(deltaTime, this);

        processCommands();

        updatePhysics();

        updateAnimation();

        updateObservers();

        log();
    }

    @Override
      public void resolveStartCollision(Fixture fixture) {

        Object objectCollidedWith = fixture.getBody().getUserData();

        if(objectCollidedWith instanceof GemSprite) {
            GameState.increaseScore();
        }
    }

    @Override
    public void resolveEndCollision(Fixture fixture) {

    }

    @Override
    public String toString() {
        return "VelX: " + getVelocity().x + " VelY: " +  getVelocity().y + " PosX: " + getX() + " PosY: " +  getY() + " isGrounded: " + isGrounded;
    }

    @Override
    public void addObserver(Observer observer) {

        observers.add(observer);
    }

    @Override
    public boolean removeObserver(Observer observer) {

        return observers.remove(observer);
    }

    @Override
    public void setCurrentAnimationByState(StateType currentAnimation) {
        this.currentAnimation = currentAnimation;
    }

    private void log() {

        if(DEV) {
            if (TimeUtils.nanoTime() - lastTime > 1000000000) {
                logger.info(this.toString());
                lastTime = TimeUtils.nanoTime();
            }
        }
    }

    private void updateObservers() {

        for(Observer observer : observers) {

            observer.update(this);
        }
    }

    private void updateAnimation() {

        sprite.setRegion(animations.get(currentAnimation).getKeyFrame(state.getStateTime()));
    }

    private void processCommands() {

        Array<Command> commands =  inputHandler.getCommands();

        for(Command command : commands) {
            command.execute(this);
            if(!command.wasHandled()) {
                break;
            }
        }
    }

    private void updatePhysics() {

        Vector2 velocity = getVelocity();

        float movementForce = 0f;

        if(getIsGrounded()
        && (getState().getStateType() != StateType.DODGE)) {

            if(getDirection() == Direction.RIGHT) {
                if (velocity.x < getMaxVelocity()) {
                    movementForce = getForceVector().x;
                }
            }
            else if (getDirection() == Direction.LEFT) {
                if (velocity.x > -getMaxVelocity()) {
                    movementForce = -getForceVector().x;
                }
            }
            else if (getDirection() == Direction.STOPPED) {
                movementForce = velocity.x * -10;
            }
        }
        else if(!getIsGrounded()) {
            if(getDirection() == Direction.RIGHT) {
                if (velocity.x < getMaxVelocity()) {
                    movementForce = getForceVector().x * MOVEMENT_DAMPENING;
                }
            }
            else if (getDirection() == Direction.LEFT) {
                if (velocity.x > -getMaxVelocity()) {
                    movementForce = -getForceVector().x * MOVEMENT_DAMPENING;
                }
            }
            else if (getDirection() == Direction.STOPPED) {
                movementForce = 0f;
            }
        }

        getBody().applyForceToCenter(movementForce, 0f, true);
    }
}

package com.daniebker.gemhunter.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import lib.sprites.Box2DSprite;

/**
 * Created by dan on 8/03/15.
 */
public class SpikeSprite extends Box2DSprite {

    public static final String NAME = "spike";

    @Override
    public float getMaxVelocity() {
        return 0f;
    }

    @Override
    public Vector2 getForceVector() { return Vector2.Zero; }

    public SpikeSprite(Body body, Sprite sprite) {
        super(body, sprite);
    }

    @Override
    public void resolveStartCollision(Fixture fixture) {
        Object userData = fixture.getBody().getUserData();

        if(userData instanceof Player) {
           Player player = (Player)userData;
            player.destroy();
        }
    }

    @Override
    public void resolveEndCollision(Fixture fixture) {

    }

}

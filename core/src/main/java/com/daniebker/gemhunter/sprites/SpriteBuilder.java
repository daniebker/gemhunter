package com.daniebker.gemhunter.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Array;
import com.daniebker.gamelib.TiledMapWorld;
import com.daniebker.gemhunter.definitions.LevelDefinitions;
import com.daniebker.gemhunter.definitions.TileProperties;
import lib.sprites.GameActor;
import lib.physics.PhysicsBodyBuilder;

import java.util.Iterator;

/**
 * Created by dan on 9/03/15.
 */
public class SpriteBuilder {

    private final PhysicsBodyBuilder physicsBodyBuilder;
    private final TiledMapWorld tiledMapWorld;
    private SpriteFactory spriteFactory;

    public SpriteBuilder(TiledMapWorld tiledMapWorld,
                         PhysicsBodyBuilder physicsBodyBuilder,
                         SpriteFactory spriteFactory) {

        this.tiledMapWorld = tiledMapWorld;
        this.physicsBodyBuilder = physicsBodyBuilder;
        this.spriteFactory = spriteFactory;
    }

    public Array<GameActor> buildSprites(String name) {
        TiledMap map = tiledMapWorld.getWorldMap();

        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get(LevelDefinitions.INTERACTIVE_LAYER);
        MapLayer gemObjects = map.getLayers().get(LevelDefinitions.INTERACTIVE_OBJECTS);

        BodyDef boxBodyDef = new BodyDef();
        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.density = 1f;
        fixtureDef.friction = 1.f;
        fixtureDef.restitution = 0.2f;

        MapObjects objects = gemObjects.getObjects();
        Iterator<MapObject> objectIt = objects.iterator();
        Array<GameActor> sprites = new Array<GameActor>();

        while (objectIt.hasNext()) {

            MapObject object = objectIt.next();
            if (object instanceof TextureMapObject) {
                continue;
            }

            if (object instanceof RectangleMapObject
                    && object.getProperties().get(TileProperties.NAME).equals(name)) {
                RectangleMapObject rect = (RectangleMapObject) object;

                Sprite sprite = createSprite(layer, rect);

                boxBodyDef.type = BodyDef.BodyType.DynamicBody;
                boxBodyDef.position.x = sprite.getX();
                boxBodyDef.position.y = sprite.getY();
                boxBodyDef.fixedRotation = false;

                Body body = physicsBodyBuilder.createBodyFromSprite(sprite, boxBodyDef, fixtureDef);
                sprites.add(spriteFactory.buildSprite(name, body, sprite));
            }
        }
        return sprites;
    }

    private Sprite createSprite(TiledMapTileLayer layer, RectangleMapObject rect) {
        float x = (rect.getRectangle().getX() + rect.getRectangle().getWidth() * .5f) / tiledMapWorld.getPixelsToWorld();
        float y = (rect.getRectangle().getY() + rect.getRectangle().getHeight() * .5f) / tiledMapWorld.getPixelsToWorld();
        float width = rect.getRectangle().width / tiledMapWorld.getPixelsToWorld();
        float height = rect.getRectangle().height / tiledMapWorld.getPixelsToWorld();

        TiledMapTileLayer.Cell cell = layer.getCell((int) x, (int) y);

        Sprite sprite = new Sprite();
        //When we retry this cell is null. Assets need to be loaded in the Asset Manager and stored
        //Assest should be build only when level is loaded
        TextureRegion texture = new TextureRegion(cell.getTile().getTextureRegion());
        sprite.setPosition(x, y);
        sprite.setSize(width, height);
        sprite.setRegion(texture);

        layer.setCell((int) x, (int) y, null);
        return sprite;
    }
}

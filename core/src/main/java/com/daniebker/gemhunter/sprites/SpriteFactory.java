package com.daniebker.gemhunter.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import lib.sprites.GameActor;

/**
 * Created by dan on 9/03/15.
 */
public class SpriteFactory {

    public GameActor buildSprite(String name, Body body, Sprite sprite) {

        if (name.equals(GemSprite.NAME)) {
            return new GemSprite(body, sprite);
        }

        if (name.equals(SpikeSprite.NAME)) {
            return new SpikeSprite(body, sprite);
        }

        return null;
    }
}

package com.daniebker.gemhunter.sprites;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import lib.sprites.Box2DSprite;

/**
 * Created by dan on 22/02/15.
 */
public class GemSprite extends Box2DSprite {

    public static final String NAME = "gem";

    @Override
    public float getMaxVelocity() {
        return 0f;
    }

    @Override
    public Vector2 getForceVector() {return Vector2.Zero; }

    public GemSprite(Body body, Sprite sprite) {
        super(body, sprite);
    }

    @Override
    public void resolveStartCollision(Fixture fixture) {

        Object userData = fixture.getBody().getUserData();

        if(userData instanceof Player) {
           destroy();
        }
    }

    @Override
    public void resolveEndCollision(Fixture fixture) {

    }
}

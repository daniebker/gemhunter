package com.daniebker.gemhunter.definitions;

/**
 * Created by dan on 29/12/14.
 */
public class TextureDefinitions {
    public static String BADLOGIC = "badlogic.jpg";
    public static String SHELL = "Shell.png";

    //PLAYER ANIMATION
    public static String IDLE0 = "prototype/player/Idle__000.png";
    public static String IDLE1 = "prototype/player/Idle__001.png";
    public static String IDLE2 = "prototype/player/Idle__002.png";
    public static String IDLE3 = "prototype/player/Idle__003.png";
    public static String IDLE4 = "prototype/player/Idle__004.png";
    public static String IDLE5 = "prototype/player/Idle__005.png";
    public static String IDLE6 = "prototype/player/Idle__006.png";
    public static String IDLE7 = "prototype/player/Idle__007.png";
    public static String IDLE8 = "prototype/player/Idle__008.png";
    public static String IDLE9 = "prototype/player/Idle__009.png";

    public static String RUN0 = "prototype/player/Run__000.png";
    public static String RUN1 = "prototype/player/Run__001.png";
    public static String RUN2 = "prototype/player/Run__002.png";
    public static String RUN3 = "prototype/player/Run__003.png";
    public static String RUN4 = "prototype/player/Run__004.png";
    public static String RUN5 = "prototype/player/Run__005.png";
    public static String RUN6 = "prototype/player/Run__006.png";
    public static String RUN7 = "prototype/player/Run__007.png";
    public static String RUN8 = "prototype/player/Run__008.png";
    public static String RUN9 = "prototype/player/Run__009.png";

    public static String JUMP0 = "prototype/player/Jump__000.png";
    public static String JUMP1 = "prototype/player/Jump__001.png";
    public static String JUMP2 = "prototype/player/Jump__002.png";
    public static String JUMP3 = "prototype/player/Jump__003.png";
    public static String JUMP4 = "prototype/player/Jump__004.png";
    public static String JUMP5 = "prototype/player/Jump__005.png";
    public static String JUMP6 = "prototype/player/Jump__006.png";
    public static String JUMP7 = "prototype/player/Jump__007.png";
    public static String JUMP8 = "prototype/player/Jump__008.png";
    public static String JUMP9 = "prototype/player/Jump__009.png";

    public static String SLIDE0 = "prototype/player/Slide__000.png";
    public static String SLIDE1 = "prototype/player/Slide__001.png";
    public static String SLIDE2 = "prototype/player/Slide__002.png";
    public static String SLIDE3 = "prototype/player/Slide__003.png";
    public static String SLIDE4 = "prototype/player/Slide__004.png";
    public static String SLIDE5 = "prototype/player/Slide__005.png";
    public static String SLIDE6 = "prototype/player/Slide__006.png";
    public static String SLIDE7 = "prototype/player/Slide__007.png";
    public static String SLIDE8 = "prototype/player/Slide__008.png";
    public static String SLIDE9 = "prototype/player/Slide__009.png";
}

package com.daniebker.gemhunter.definitions;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dan on 29/12/14.
 */
public class AnimationDefinitions {

    public static final String RUN = "Run";
    public static final String IDLE = "idle";
    public static final String JUMP = "jump";
    public static final String SLIDE = "slide";

    private Map<String, AnimationDefinition> definitions = new HashMap<String, AnimationDefinition>(1);

    public AnimationDefinitions(AssetManager assetManager) {
        Array<TextureRegion> idleFrames = new Array<TextureRegion>();
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE0, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE1, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE2, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE3, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE4, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE5, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE6, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE7, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE8, Texture.class)));
        idleFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.IDLE9, Texture.class)));
        definitions.put(IDLE, new AnimationDefinition(IDLE, idleFrames, PlayMode.LOOP, 0.09f));

        Array<TextureRegion> runFrames = new Array<TextureRegion>();
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN0, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN1, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN2, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN3, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN4, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN5, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN6, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN7, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN8, Texture.class)));
        runFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.RUN9, Texture.class)));
        definitions.put(RUN, new AnimationDefinition(RUN, runFrames, PlayMode.LOOP, 0.07f));

        Array<TextureRegion> jumpFrames = new Array<TextureRegion>();
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP0, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP1, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP2, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP3, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP4, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP5, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP6, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP7, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP8, Texture.class)));
        jumpFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.JUMP9, Texture.class)));
        definitions.put(JUMP, new AnimationDefinition(JUMP, jumpFrames, PlayMode.NORMAL, 0.07f));

        Array<TextureRegion> slideFrames = new Array<TextureRegion>();
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE0, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE1, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE2, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE3, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE4, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE5, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE6, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE7, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE8, Texture.class)));
        slideFrames.add(new TextureRegion(assetManager.get(TextureDefinitions.SLIDE9, Texture.class)));
        definitions.put(SLIDE, new AnimationDefinition(SLIDE, slideFrames, PlayMode.NORMAL, 0.07f));
    }

    public AnimationDefinition getDefinition(String name) {
        return definitions.get(name);
    }
}

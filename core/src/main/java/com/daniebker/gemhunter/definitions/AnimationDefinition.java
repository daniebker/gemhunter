package com.daniebker.gemhunter.definitions;

import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Created by dan on 29/12/14.
 */
public class AnimationDefinition {

    private String name;
    private Array<TextureRegion> frames;
    private PlayMode playMode;
    private float playSpeed;

    public AnimationDefinition(String name, Array<TextureRegion> frames, PlayMode playMode, float playSpeed) {
        this.name = name;
        this.frames = frames;
        this.playMode = playMode;
        this.playSpeed = playSpeed;
    }

    public String getName() {
        return name;
    }

    public Array<TextureRegion> getFrames() {
        return frames;
    }


    public PlayMode getPlayMode() {
        return playMode;
    }

    public float getPlaySpeed() {
        return playSpeed;
    }
}

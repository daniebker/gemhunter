package com.daniebker.gemhunter.definitions;

/**
 * Created by dan on 30/12/14.
 */
public class LevelDefinitions {

    public static final String LEVEL3 = "levels/level3.tmx";
    public static String LEVEL1 = "levels/level1.tmx";
    public static String LEVEL2 = "levels/level2.tmx";

    public static String PHYSICS_LAYER = "physics";
    public static String INTERACTIVE_LAYER = "interactive";
    public static String INTERACTIVE_OBJECTS = "interactiveobjects";
}

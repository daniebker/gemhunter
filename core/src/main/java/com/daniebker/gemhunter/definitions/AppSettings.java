package com.daniebker.gemhunter.definitions;

import com.badlogic.gdx.utils.Logger;

/**
 * Created by dan on 30/12/14.
 */
public class AppSettings {

    public static boolean DEV = true;
    public static int LOGGING_LEVEL = Logger.DEBUG;
}

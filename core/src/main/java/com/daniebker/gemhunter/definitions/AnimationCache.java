package com.daniebker.gemhunter.definitions;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.google.inject.Singleton;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by dan on 29/12/14.
 */
@Singleton
public class AnimationCache {

    private final AnimationDefinitions definitions;
    private Map<String, Animation>animations = new HashMap<String, Animation>();

    public AnimationCache(AssetManager assetManager) {

        definitions = new AnimationDefinitions(assetManager);
    }

    public Animation getAnimation(String name) {

        if(!animations.containsKey(name)) {
            AnimationDefinition definition = definitions.getDefinition(name);
            Animation animation = new Animation(definition.getPlaySpeed(), definition.getFrames(), definition.getPlayMode());
            animations.put(name, animation);
            return animation;
        }

        return animations.get(name);
    }


}

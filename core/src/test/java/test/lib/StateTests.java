package test.lib;

import com.daniebker.gemhunter.states.IdleState;
import com.daniebker.gemhunter.states.RunState;
import lib.states.State;
import org.junit.Test;

import static junit.framework.Assert.assertNotSame;
import static junit.framework.TestCase.assertEquals;

/**
 * Created by dan on 22/02/15.
 */
public class StateTests {

    @Test
    public void testEquals_AreEqual_ReturnsTrue()  {

        State stateOne = new RunState();
        State stateTwo = new RunState();

        assertEquals(stateOne, stateTwo);
    }

    @Test
    public void testEquals_AreNotEqual_ReturnsFalse()  {

        State stateOne = new RunState();
        State stateTwo = new IdleState();

        assertNotSame(stateOne, stateTwo);
    }

    @Test
    public void testEquals_AreNotSameType_ReturnsFalse()  {

        State stateOne = new RunState();
        Object different = new String();

        assertNotSame(stateOne, different);
    }
}

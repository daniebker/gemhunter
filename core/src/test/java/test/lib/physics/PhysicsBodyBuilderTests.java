package test.lib.physics;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import lib.physics.PhysicsBodyBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import test.GdxTestRunner;

import static junit.framework.TestCase.assertEquals;

import static org.mockito.Mockito.*;

/**
 * Created by dan on 21/02/15.
 */
@RunWith(GdxTestRunner.class)
public class PhysicsBodyBuilderTests {

    private PhysicsBodyBuilder physicsBodyBuilder;
    private Sprite spriteMock;

    @Before
    public void setUp() {
        World worldMock = new World(new Vector2(.0f, .0f), true);

        physicsBodyBuilder = new PhysicsBodyBuilder(worldMock);

        spriteMock = mock(Sprite.class);

        stub(spriteMock.getWidth()).toReturn(10f);
        stub(spriteMock.getHeight()).toReturn(10f);
    }

    @Test
    public void testCreateBodyFromSprite_CreatesBodyAtCorrectPosition() {

        Vector2 expectedPosition = new Vector2(0f, 0f);

        assertEquals(expectedPosition, getDynamicBody().getPosition());
    }


    @Test
    public void testCreateBodyFromSprite_CallsWidthAndHeight() {
        getDynamicBody();
        verify(spriteMock, times(1)).getHeight();
        verify(spriteMock, times(1)).getWidth();
    }

    @Test
    public void testCreateBodyFromSprite_CreatesDynamicBody() {

        assertEquals(BodyDef.BodyType.DynamicBody, getDynamicBody().getType());
    }

    @Test
    public void testCreateBodyFromSprite_CreatesNonFixedRotation() {

        assertEquals(false, getDynamicBody().isFixedRotation());
    }

    private Body getDynamicBody() {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;

        return physicsBodyBuilder.createBodyFromSprite(spriteMock, bodyDef, new FixtureDef());
    }
}

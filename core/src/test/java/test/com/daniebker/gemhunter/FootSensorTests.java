package test.com.daniebker.gemhunter;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.daniebker.gemhunter.FootSensor;
import com.daniebker.gemhunter.sprites.GemSprite;
import lib.sprites.GameActor;
import lib.states.State;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * FootSensorTests
 * dan - 6/04/15
 */
public class FootSensorTests {

    @Test
    public void testResolveStartColision_WithGem_HasNoContacts() {

        GameActor gameActorMock = mock(GameActor.class);
        State stateMock = mock(State.class);
        stub(gameActorMock.getState()).toReturn(stateMock);

        FootSensor footSensor = new FootSensor(gameActorMock);

        Fixture fixtureMock = mock(Fixture.class);
        GameActor gemMock = mock(GemSprite.class);
        Body bodyMock = mock(Body.class);
        stub(fixtureMock.getBody()).toReturn(bodyMock);
        stub(bodyMock.getUserData()).toReturn(gemMock);

        footSensor.resolveStartCollision(fixtureMock);

        verify(gameActorMock, times(0)).setIsGrounded(true);
    }
}

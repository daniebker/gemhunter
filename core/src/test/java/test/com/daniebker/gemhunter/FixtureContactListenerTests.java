package test.com.daniebker.gemhunter;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.daniebker.gemhunter.FixtureContactListener;
import com.daniebker.gemhunter.sprites.Player;
import lib.states.State;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.*;

/**
 * Created by dan on 22/02/15.
 */
public class FixtureContactListenerTests {

    private Player playerMock;
    private Contact contact;
    private FixtureContactListener fixtureContactListener;
    private Fixture fixtureAMock;
    private Fixture fixtureBMock;
    private State stateMock;
    private Body bodyWithUserData;
    private Body bodyWithNoUserDataMock;

    @Before
    public void setUp() {

        fixtureContactListener = new FixtureContactListener();

        stateMock = mock(State.class);

        playerMock = mock(Player.class);
        stub(playerMock.getState()).toReturn(stateMock);

        bodyWithUserData = mock(Body.class);
        stub(bodyWithUserData.getUserData()).toReturn(playerMock);

        fixtureAMock = mock(Fixture.class);
        stub(fixtureAMock.getBody()).toReturn(bodyWithUserData);

        bodyWithNoUserDataMock = mock(Body.class);
        stub(bodyWithNoUserDataMock.getUserData()).toReturn(null);

        fixtureBMock = mock(Fixture.class);
        stub(fixtureBMock.getBody()).toReturn(bodyWithNoUserDataMock);

        contact = mock(Contact.class);
        stub(contact.getFixtureA()).toReturn(fixtureAMock);
        stub(contact.getFixtureB()).toReturn(fixtureBMock);
        stub(contact.isTouching()).toReturn(false);
    }

    @Test
    public void testBeginContact_WhenFixtureABodyUserDataIsPlayer_CallsResolveCollision() {

        fixtureContactListener.beginContact(contact);

        verify(playerMock, times(1)).resolveStartCollision(fixtureBMock);
    }

    @Test
    public void testBeginContact_WhenFixtureBBodyUserDataIsPlayer_CallsResolveCollision() {

        stub(fixtureAMock.getBody()).toReturn(bodyWithNoUserDataMock);
        stub(fixtureBMock.getBody()).toReturn(bodyWithUserData);
        fixtureContactListener.beginContact(contact);

        verify(playerMock, times(1)).resolveStartCollision(fixtureAMock);
    }

    @Test
    public void testBeginContact_WhenFixtureAUserDataIsPlayer_CallsResolveCollision() {

        stub(fixtureAMock.getBody()).toReturn(bodyWithNoUserDataMock);
        stub(fixtureAMock.getUserData()).toReturn(playerMock);

        fixtureContactListener.beginContact(contact);

        verify(playerMock, times(1)).resolveStartCollision(fixtureBMock);
    }

    @Test
    public void testBeginContact_WhenFixtureBUserDataIsPlayer_CallsResolveCollision() {

        stub(fixtureAMock.getBody()).toReturn(bodyWithNoUserDataMock);
        stub(fixtureBMock.getUserData()).toReturn(playerMock);

        fixtureContactListener.beginContact(contact);

        verify(playerMock, times(1)).resolveStartCollision(fixtureAMock);
    }

    @Test
    public void testEndContact_WhenFixtureABodyUserDataIsPlayer_CallsResolveCollision() {

        fixtureContactListener.endContact(contact);

        verify(playerMock, times(1)).resolveEndCollision(fixtureBMock);
    }

    @Test
    public void testEndContact_WhenFixtureBBodyUserDataIsPlayer_CallsResolveCollision() {

        stub(fixtureAMock.getBody()).toReturn(bodyWithNoUserDataMock);
        stub(fixtureBMock.getBody()).toReturn(bodyWithUserData);
        fixtureContactListener.endContact(contact);

        verify(playerMock, times(1)).resolveEndCollision(fixtureAMock);
    }

    @Test
    public void testEndContact_WhenFixtureAUserDataIsPlayer_CallsResolveCollision() {

        stub(fixtureAMock.getBody()).toReturn(bodyWithNoUserDataMock);
        stub(fixtureAMock.getUserData()).toReturn(playerMock);

        fixtureContactListener.endContact(contact);

        verify(playerMock, times(1)).resolveEndCollision(fixtureBMock);
    }

    @Test
    public void testEndContact_WhenFixtureBUserDataIsPlayer_CallsResolveCollision() {

        stub(fixtureAMock.getBody()).toReturn(bodyWithNoUserDataMock);
        stub(fixtureBMock.getUserData()).toReturn(playerMock);

        fixtureContactListener.endContact(contact);

        verify(playerMock, times(1)).resolveEndCollision(fixtureAMock);
    }


    @Test
    public void testEndContact_ContactIsNotTouching_DoesNothing() {

       stub(contact.isTouching()).toReturn(true);

        fixtureContactListener.endContact(contact);

        verify(playerMock, never()).resolveEndCollision(fixtureAMock);
        verify(playerMock, never()).resolveEndCollision(fixtureBMock);
    }
}

package test.com.daniebker.gemhunter;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.daniebker.gemhunter.Director;
import com.daniebker.gemhunter.GameState;
import com.daniebker.gemhunter.sprites.GemSprite;
import com.daniebker.gemhunter.sprites.Player;
import com.daniebker.gemhunter.states.StateType;
import lib.input.InputHandler;
import lib.sprites.GameActor;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.stub;

import static org.mockito.Mockito.any;

/**
 * Created by dan on 23/02/15.
 */
public class PlayerTests {

    private Player player;
    private Sprite spriteMock;
    private Fixture fixtureMock;

    @Before
    public void setUp() {
        fixtureMock = mock(Fixture.class);
        spriteMock = mock(Sprite.class);

        InputHandler inputHandlerMock = mock(InputHandler.class);
        Body bodyMock =  mock(Body.class);
        Director directorMock = mock(Director.class);

        World world = new World(new Vector2(0.0f, 0.0f), false);

        GameActor gem = new GemSprite(bodyMock, spriteMock);

        stub(bodyMock.getUserData()).toReturn(gem);
        stub(fixtureMock.getBody()).toReturn(bodyMock);

        stub(bodyMock
                .createFixture(any(FixtureDef.class)))
                .toReturn(fixtureMock);

        player = new Player(new HashMap<StateType, Animation>(), spriteMock, bodyMock, inputHandlerMock, directorMock);
    }

    @Test
    public void testResolveCollision_WithGem_IncreasesGemCount() {

        player.resolveStartCollision(fixtureMock);

        assertEquals(1, GameState.getScore());
    }
}

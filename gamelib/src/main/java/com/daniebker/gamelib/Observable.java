package com.daniebker.gamelib;

/**
 * Interface for implementing an
 * observable object
 */
public interface Observable {

    /**
     * Adds an observer to the list of observers
     * @param observer the observer to add
     */
    void addObserver(Observer observer);

    /**
     * Remove the observer from the list of observers
     * @param observer the observer to remove
     * @return True if the observer was removed
     */
    boolean removeObserver(Observer observer);
}
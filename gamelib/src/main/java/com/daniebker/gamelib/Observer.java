package com.daniebker.gamelib;


import com.daniebker.gamelib.Observable;

/**
 * Interface to implement the observer pattern
 */
public interface Observer {

    /**
     * Update method for an observable to notify
     * the observers
     * @param observable The class we're observing
     */
    void update(Observable observable);
}
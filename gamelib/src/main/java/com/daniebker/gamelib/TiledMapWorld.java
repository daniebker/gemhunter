package com.daniebker.gamelib;

import com.badlogic.gdx.maps.MapProperties;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

/**
 * TiledMapWorld
 * dan - 6/04/15
 */
public class TiledMapWorld {

    private final MapProperties mapProperties;
    private Vector2 gravity;
    private float pixelsToWorld;
    private int worldHeight;
    private TiledMap map;
    private World world;
    private int worldWidth;

    public TiledMapWorld(TiledMap map) {

        this.map = map;
        this.mapProperties = map.getProperties();

        this.worldHeight = mapProperties.get("height", Integer.class);
        this.worldWidth = mapProperties.get("width", Integer.class);
        this.pixelsToWorld = Float.parseFloat(
                    mapProperties.get("pixelsToWorld", String.class));
    }

    public int getWorldHeight() {
        return worldHeight;
    }

    public Vector2 getWorldGravity() {

        if (gravity == null) {
            gravity = new Vector2(
                    Float.parseFloat(mapProperties.get("gravityX", String.class)),
                    Float.parseFloat(mapProperties.get("gravityY", String.class)));
        }

        return gravity;
    }

    public float getPixelsToWorld() {
        return pixelsToWorld;
    }

    public TiledMap getWorldMap() {
        return map;
    }

    public World getWorld() {
        if(world == null) {
            world = new World(getWorldGravity(), true);
        }

        return world;
    }

    public int getWorldWidth() {
        return worldWidth;
    }
}

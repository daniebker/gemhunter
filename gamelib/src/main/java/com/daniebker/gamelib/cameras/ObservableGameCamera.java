package com.daniebker.gamelib.cameras;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.daniebker.gamelib.Observer;

/**
 * Created by dan on 14/03/15.
 */
public interface ObservableGameCamera extends Observer {

    void reset();

    OrthographicCamera getOrthographicCamera();

    void translate(int x, int y, int z);

    void rotate(float speed, int x, int y, int z);

    float getZoom();

    float getViewportWidth();

    float getViewportHeight();

    void zoom(float v);

    Vector3 getPosition();

    float getScreenWidth();

    float getScreenHeight();
}

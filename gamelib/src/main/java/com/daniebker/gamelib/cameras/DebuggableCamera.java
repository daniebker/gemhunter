package com.daniebker.gamelib.cameras;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;
import com.daniebker.gamelib.Observable;

/**
 * DebugableCamera
 * dan - 6/04/15
 */
public class DebuggableCamera implements InputProcessor, ObservableGameCamera {

    private ObservableGameCamera camera;

    public DebuggableCamera(ObservableGameCamera camera) {

        this.camera = camera;
    }

    @Override
    public boolean keyDown(int keycode) {
        float rotationSpeed = .3f;
        boolean handled = false;

        if (keycode == Input.Keys.E) {
        	zoom(getZoom() + 0.02f);
            handled = true;
        }
        else if(keycode == Input.Keys.Q) {
        	zoom(getZoom() + -0.02f);
            handled =true;
        }
        else if(keycode == Input.Keys.TAB) {
        	rotate(-rotationSpeed, 0, 0, 1);
            handled =true;
        }
        else if(keycode == Input.Keys.R) {
        	rotate(rotationSpeed, 0, 0, 1);
            handled =true;
        }

        zoom(MathUtils.clamp(getZoom(), 0.1f, 100 / getViewportWidth()));

        return handled;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void reset() {

    }

    @Override
    public OrthographicCamera getOrthographicCamera() {
        return camera.getOrthographicCamera();
    }

    @Override
    public void translate(int x, int y, int z) {
        camera.translate(x,y,z);
    }

    @Override
    public void rotate(float speed, int x, int y, int z) {
        camera.rotate(speed, x, y, z);
    }

    @Override
    public float getZoom() {
        return camera.getZoom();
    }

    @Override
    public float getViewportWidth() {
        return camera.getViewportWidth();
    }

    @Override
    public float getViewportHeight() {
        return camera.getViewportHeight();
    }

    @Override
    public void zoom(float v) {
        camera.zoom(v);
    }

    @Override
    public Vector3 getPosition() {
        return camera.getPosition();
    }

    @Override
    public void update(Observable observable) {
        camera.update(observable);
    }

    @Override
    public float getScreenWidth() {
        return camera.getScreenWidth();
    }

    @Override
    public float getScreenHeight() {
        return camera.getScreenWidth();
    }
}
